import React from 'react';
import './Set.css';
import { Link } from "react-router-dom";

function Set() {
  return (
    <div className="Content">
      <h1>This is the SET Screen</h1>
      <Link to="/">Home</Link>
    </div>
  );
}

export default Set;
