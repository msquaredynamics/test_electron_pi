export const initialState = {
  menuVisible: false,
}

export default function appReducer(state = initialState, action) {
  console.log('app reducer called:', action);
  switch (action.type) {
    case 'SHOW_MENU':
       return {
         ...state,
         menuVisible: true
       }
  
    case 'HIDE_MENU':
      return {
        ...state,
        menuVisible: false
      } 
    
    default: return state;
  }
} 