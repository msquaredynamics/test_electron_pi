import React from 'react';
import './Toolbar.css';


function Toolbar(props) {

  return (
    <div className="Toolbar">
      <h1 onClick={props.clickHandler}>{props.title}</h1>
    </div>
  );
}

export default Toolbar;