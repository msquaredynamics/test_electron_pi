import React from 'react';
import './App.css'; 
import NavMenu from './NavMenu/NavMenu';
import Toolbar from './Toolbar/Toolbar';
import Home from './Screens/Home/Home';
import Set from './Screens/Set/Set';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { connect } from 'react-redux'
import { AnimatedSwitch } from 'react-router-transition';


function mapStyles(styles) {
  return {
    opacity: styles.opacity,
    transform: `translateY(${styles.translate}px)`,
  };
}


// child matches will...
const translateTransition = {
  // start in a transparent, upscaled state
  atEnter: {
    opacity: 0,
    translate: -30,
  },
  // leave in a transparent, downscaled state
  atLeave: {
    opacity: 0,
    translate: 0,
  },
  // and rest at an opaque, normally-scaled state
  atActive: {
    opacity: 1,
    translate: 0,
  },
};


class App extends React.Component {

  onToolbarClick = () => {
    if (this.props.menuVisible)
      this.props.hideMenu();
    else
      this.props.showMenu();
  }

  onOverlayClick = () => {
    if (this.props.menuVisible) {
      this.onToolbarClick();
    }
  }

  render() {
    return (
      <div className="App">
      <NavMenu showMenu={this.props.menuVisible} />
      <div style={{ 
        position: "absolute",
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
        opacity: this.props.menuVisible ? '0.7' : '0',
        visibility: this.props.menuVisible ? 'visible' : 'hidden',
        transition: 'opacity 0.5s'
      }} onClick={this.onOverlayClick}></div>
        
      
      <Toolbar title="Cool Toolbar" clickHandler={this.onToolbarClick}/>
      <Router>
        <AnimatedSwitch
        atEnter={translateTransition.atEnter}
        atLeave={translateTransition.atLeave}
        atActive={translateTransition.atActive}
        mapStyles={mapStyles}
        className="switch-wrapper"
        >
          <Route path="/" exact component={Home} />
          <Route path="/set" exact component={Set} />
        </AnimatedSwitch>
      </Router>
    
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    menuVisible: state.menuVisible
  }
}


const mapDispatchToProps = { 
  showMenu : () => ({type: 'SHOW_MENU', payload: '' }),
  hideMenu: () => ({ type: 'HIDE_MENU', payload: '' })
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
