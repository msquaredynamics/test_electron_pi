const redux = require('redux');
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');
const isDev = require('electron-is-dev');

const ele_redux = require('electron-redux');


let initialState = {
  menuVisible: true,
}

function appReducer(state = initialState, action) {

  console.log('electron reducer called:', action);
  switch (action.type) {
    case 'SHOW_MENU':
       return {
         ...state,
         menuVisible: true
       }
  
    case 'HIDE_MENU':
      return {
        ...state,
        menuVisible: false
      } 
    
    default: return state;
  }
} 

// Note: passing enhancer as the last argument to createStore requires redux@>=3.1.0
let store = redux.createStore(appReducer, initialState, redux.applyMiddleware(
  ele_redux.forwardToRenderer, // Last one
));

ele_redux.replayActionMain(store);

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 480, 
    height: 800, 
    frame: false, 
    webPreferences: {
      nodeIntegration: true
    }
  });

  mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
  mainWindow.on('closed', () => mainWindow = null);
  mainWindow.setMenuBarVisibility(false);
}

app.on('ready', createWindow);
app.on('browser-window-created',function(e,window) {
  window.setMenu(null);
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});